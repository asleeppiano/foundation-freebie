/*eslint no-undef: "error"*/
/*eslint-env node*/
module.exports = {
  theme: {
    extend: {
      backgroundColor: {
        'primary': 'rgba(243,198,189,1)',
        'secondary': 'rgba(48,64,196,1)',
        'inbox': 'rgba(252,240,227, 0.4)',
        'engage': 'rgba(0, 161, 198, 0.07)',
        'silver': 'rgba(43,41,45,0.2)'
      },
      textColor:{
        'secondary': 'rgba(48,64,196,1)',
        'accent': 'rgba(0, 155, 77, 1)',
        'inbox': 'rgba(241, 89, 43, 1)',
        'silver': 'rgba(43,41,45, 1)',
        'light': 'rgba(43, 41, 45, 0.6)'
      },
      maxWidth: {
        '1/4': '25%',
        '1/2': '50%',
        '3/4': '75%',
      }
    }
  },
  variants: {},
  plugins: [],
}
