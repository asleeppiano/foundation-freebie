const carouselItems = document.querySelectorAll('.carousel-item')

const carouselRadios = document.querySelectorAll('.carousel-radios label input')
carouselRadios.forEach(radio => {
  let interval = undefined
  radio.addEventListener('click', () => {
    if (radio.hasAttribute('checked')) {
      return
    }
    if (interval) {
      clearTimeout(interval)
    }
    const carouselActiveItem = document.querySelector('.carousel-item.active')
    const radioChecked = document.querySelector('.carousel-radios input[checked]')
    if (radio.dataset.slideTo > radioChecked.dataset.slideTo) {
      const nextItem = carouselItems[radio.dataset.slideTo]
      nextItem.style.transform = 'translateX(100%)'
      carouselActiveItem.style.transform = 'translateX(-100%)'

      interval = setTimeout(() => {
        carouselActiveItem.style.transform = 'none'
        nextItem.style.transform = 'none'
        carouselActiveItem.classList.remove('active')
      }, 300)
    } else {
      const prevItem = carouselItems[radio.dataset.slideTo]
      prevItem.style.transform = 'translateX(-100%)'
      carouselActiveItem.style.transform = 'translateX(100%)'

      interval = setTimeout(() => {
        prevItem.style.transform = 'none'
        carouselActiveItem.style.transform = 'none'
        carouselActiveItem.classList.remove('active')
      }, 300)
    }
    carouselItems[radio.dataset.slideTo].classList.add('active')
    radioChecked.removeAttribute('checked')
    radio.setAttribute('checked', '')
  })
})

const listItems = document.querySelectorAll('.list-item')

listItems.forEach(item => {
  item.firstElementChild.addEventListener('click', () => {
    if (item.classList.contains('active')) {
      return
    }
    const currentActive = document.querySelector('.list-item.active')
    const currentActivePara = document.querySelector('.list-item.active p')
    item.classList.add('active')
    currentActive.classList.remove('active')
    item.lastElementChild.style.display = 'block'
    currentActivePara.style.display = 'none'
  })
})
